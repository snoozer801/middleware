(function() {

    window.onload = function() {
        if (window.$){
            console.dir(document.cookie);
            let jQuery = window.$;
            let updateButtons = jQuery('button[id*="edit"]');
            let errorBox = jQuery('.error-box');
            console.log('ready to use jquery');
            updateButtons.on('click', function(){
                let updateButton = jQuery(this);
                if (updateButton.hasClass('updating')){
                    let result = updateButton.prev('input').attr('placeholder').replace(' ', '').match(/(\w+.+\w+)/g);
                    console.log(result);
                    if (result) {
                        jQuery.ajax({
                            url: '/api/users/',
                            method: 'PUT',
                            data: {
                                search: result[0],
                                update: updateButton.prev('input').val(),
                                field: updateButton.parent().attr('id')
                            },
                            success: function (data) {
                                if (!data.error && !data.errmsg) {
                                    updateButton.prev('input').remove();
                                    updateButton.parent().contents()[0].textContent += data.update;
                                    updateButton.removeClass('updating');
                                    updateButton.text('Update');
                                    jQuery('#updated').contents()[0].textContent = 'Last Updated: ' + new Date(Date.now());
                                    updateButton.next('button').remove(); //@TODO: include a count of updates, only allowing the user to update a few times
                                    // will prevent brute force attacks
                                } else {
                                    if (data.errmsg) {
                                        alert(data.errmsg)
                                    } else {
                                        alert(data.error + ' ' + updateButton.parent().attr('id') + ' to ' + updateButton.prev('input').val() + '.');
                                    }
                                }
                            }
                        })
                    } else {

                    };
                } else {
                    updateButton.addClass('updating');
                    let targetUpdate = jQuery(this).parent('div');
                    console.log(targetUpdate);
                    let updateString = targetUpdate.contents()[0].textContent.split(': ');
                    jQuery(targetUpdate).contents()[0].textContent = updateString[0] + ': ';
                    updateButton.before('<input id="usernameUpdate" type="text" placeholder=" ['+ updateString[1] +']" </input>');
                    updateButton.text('Save');
                    updateButton.after('<button id="'+ updateString[0] + 'Cancel"> Cancel </button>');
                    jQuery('button#'+updateString[0]+'Cancel').on('click', function(){
                        console.log('cancelling');
                        updateButton.prev('input').remove();
                        updateButton.parent().contents()[0].textContent += updateString[1];
                        updateButton.removeClass('updating');
                        updateButton.text('Update');
                        this.remove();
                    });
                }
            });
            document.addEventListener("keyup", function(event) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Number 13 is the "Enter" key on the keyboard
                if (event.keyCode === 13) {
                    // Trigger the button element with a click
                    jQuery('.updating').click();
                }
            });
        }
    }
})();
