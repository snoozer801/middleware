(function() {

    let registry = function() {
        if (window.$){
            let jQuery = window.$;
            let submitButton = jQuery('#submit');
            let usernameField = jQuery('[name="username"]');
            let emailField = jQuery('[name="email"]');
            let passwordField = jQuery('[name="password"]');
            let passwordField2 = jQuery('[name="confirmPassword"]');
            let errorMessage = jQuery('#error');
            submitButton.on('click', function(){
                errorMessage.removeClass('show-error');
                errorMessage.addClass('hide-error');
                if (usernameField.val().length < 8){
                    errorMessage.text('Username must be at least 8 characters long.');
                }
                if (passwordField.val() === passwordField2.val()) {
                    submitButton.addClass('submitting');
                    let submission = {
                        username: usernameField.val(),
                        email: emailField.val(),
                        password: passwordField.val()
                    };

                    jQuery.ajax({
                        url: '/register',
                        method: 'POST',
                        data: submission,
                        success: function (data) {
                            if (data.error) {
                                if (typeof data.error === 'object'){
                                    data.error = data.error._message + '.';
                                }
                                errorMessage.text(data.error);
                                errorMessage.removeClass('hide-error');
                                errorMessage.addClass('show-error');
                                passwordField.val('');
                                passwordField2.val('');
                            } else if (data.success.authorized) {
                                window.location.assign('/home');
                            }
                            console.log(data);
                        }
                    })
                } else {
                    errorMessage.text('Passwords do not match.');
                    passwordField.val('');
                    passwordField2.val('');
                }
            });

            document.addEventListener("keyup", function(event) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Number 13 is the "Enter" key on the keyboard
                if (event.keyCode === 13) {
                    // Trigger the button element with a click
                    submitButton.click();
                }
            });
        }
    }
    if (window.addEventListener)
    {
        window.addEventListener('load', registry, false); // NB **not** 'onload'
    }
    else if (window.attachEvent)
    {
        window.attachEvent('onload', registry);
    }
})();