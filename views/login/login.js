(function() {

window.onload = function() {
    if (window.$){
        let jQuery = window.$;
        let submitButton = jQuery('button[type="submit"]');
        let errorBox = jQuery('.error-box');
        submitButton.on('click', function(){
            //errorBox.removeClass('show-error');
            //errorBox.addClass('hide-error');
            let submitButton = jQuery(this);
            let usernameField = jQuery('[name="username"]');
            let passwordField = jQuery('[name="password"]');
            submitButton.addClass('submitting');
            let submission = {
                username: usernameField.val(),
                password: passwordField.val()
            };

            jQuery.ajax({
                url: '/api/users/login',
                method: 'POST',
                data: submission,
                success: function (data) {
                if (data.error){
                    errorBox.text(data.error);
                    if (errorBox.hasClass('hide-error')) errorBox.removeClass('hide-error');
                    if (!errorBox.hasClass('show-error')) errorBox.addClass('show-error');
                    passwordField.val('');
                } else if (data.success.authorized){
                    window.location.assign('/home');
                }
                console.log(data);
                },
                error: function (err){
                    console.dir(err.responseJSON.error);
                    errorBox.text(err.responseJSON.error);
                    errorBox.removeClass('hide-error');
                    errorBox.addClass('show-error');
                    passwordField.val('');
                }
            })
        });
        document.addEventListener("keyup", function(event) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Trigger the button element with a click
                submitButton.click();
            }
        });
        }
    }
})();
