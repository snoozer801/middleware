if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}
var container, stats, controls;
var camera, scene, renderer, light;
var clock = new THREE.Clock();
var mixers = [];
init();
animate();
function init() {
    window.removeEventListener('scroll', window.scrollTo( 0, 0));
    container = document.getElementById('main');
    document.body.appendChild( container );
    console.log(container.scrollWidth + ' ' + container.scrollHeight);
    camera = new THREE.PerspectiveCamera( 45, container.scrollWidth / container.scrollHeight, 1, 2000 );
    camera.position.set( 100, 400, 600 );
    controls = new THREE.OrbitControls( camera );
    controls.target.set( 0, 200, 0 );
    controls.update();
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xa0a0a0 );
    scene.fog = new THREE.Fog( 0xa0a0a0, 200, 1000 );
    light = new THREE.HemisphereLight( 0xffffff, 0x444444 );
    light.position.set( 0, 200, 0 );
    scene.add( light );
    light = new THREE.DirectionalLight( 0xffffff );
    light.position.set( 0, 200, 100 );
    light.castShadow = true;
    light.shadow.camera.top = 360;
    light.shadow.camera.bottom = -200;
    light.shadow.camera.left = -240;
    light.shadow.camera.right = 240;
    scene.add( light );
    // scene.add( new THREE.CameraHelper( light.shadow.camera ) );
    // ground
    var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 2000, 2000 ), new THREE.MeshPhongMaterial( { color: 0x999999, depthWrite: false } ) );
    mesh.rotation.x = - Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add( mesh );
    var grid = new THREE.GridHelper( 2000, 20, 0x000000, 0x000000 );
    grid.material.opacity = 0.2;
    grid.material.transparent = true;
    scene.add( grid );
    // model
    var loader = new THREE.FBXLoader();
    console.log('loading fbx');
    loader.load( 'Spider_v2.fbx', function ( object ) {
        object.mixer = new THREE.AnimationMixer( object );
        mixers.push( object.mixer );
        var action = object.mixer.clipAction( object.animations[ 0 ] );
        action.play();
        object.traverse( function ( child ) {
            if ( child.isMesh ) {
                child.castShadow = true;
                child.receiveShadow = true;
            }
        } );
        scene.add( object );
    } );
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.scrollWidth, container.scrollHeight );
    renderer.shadowMap.enabled = true;
    container.appendChild( renderer.domElement );
    window.addEventListener( 'resize', onWindowResize, false );
    // stats
    stats = new Stats();
    container.appendChild( stats.dom );
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( container.style.width , container.style.height );
}
//
function animate() {
    requestAnimationFrame( animate );
    if ( mixers.length > 0 ) {
        for ( var i = 0; i < mixers.length; i ++ ) {
            mixers[ i ].update( clock.getDelta() );
        }
    }
    renderer.render( scene, camera );
    stats.update();
}