window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    console.log('window check', check);
    return check;
};

var isMobile = window.mobilecheck();

var Example = Example || {};

Example.timescale = function() {
    var Engine = Matter.Engine,
        Render = Matter.Render,
        Runner = Matter.Runner,
        Body = Matter.Body,
        Events = Matter.Events,
        Composite = Matter.Composite,
        Composites = Matter.Composites,
        Common = Matter.Common,
        MouseConstraint = Matter.MouseConstraint,
        Mouse = Matter.Mouse,
        World = Matter.World,
        Bodies = Matter.Bodies;

    // create engine
    var engine = Engine.create(),
        world = engine.world;

    // create renderer
    var container = document.getElementById('matter-container');
    var containerHeight = container.offsetHeight;
    var containerWidth = container.offsetWidth;
    console.log('container variables: width - ' + containerWidth + ' height - ' + containerHeight);
    var render = Render.create({
        element: container,
        engine: engine,
        options: {
            width: container.offsetWidth,
            height: container.offsetHeight,
            showAngleIndicator: false,
            wireframes: false,
            background: '#0f0f13'
        }
    });

    Render.run(render);

    // create runner
    var runner = Runner.create();
    Runner.run(runner, engine);

    var offsetY = containerHeight * 0.27;
    var offsetX = containerWidth * 0.25;
    console.log('offset Y - ', offsetY);
    console.log('offset X - ', offsetX);
    // add bodies
    World.add(world, [
        Bodies.rectangle(containerWidth / 2, -offsetY, containerWidth * 2, 20, { isStatic: true }), // top
        Bodies.rectangle(offsetX, isMobile === true ? containerHeight : containerHeight * 1.29, containerWidth * 2, 20, { isStatic: true }), // bottom
        Bodies.rectangle(isMobile === true ? containerWidth * 1.25 : containerWidth, 600, 20, containerHeight * 1.25, { isStatic: true }), // right
        Bodies.rectangle(isMobile === true ? 0 : -containerWidth * 0.33, offsetY, 20, containerHeight * 2, { isStatic: true }), // left
    ]);

    //world.bodies = [];
    var counter = 0;
    var stack = Composites.stack(isMobile === true ? 0 : -offsetX, isMobile === true ? -offsetY / 2 : 0, isMobile === true ? 3 : 6, isMobile === true ? 6 : 3, 0, 0, function(x, y) {
            counter++;
            if (counter === 1) {
                return Bodies.polygon(x, y, 5, 170, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../gitkraken-380.png'
                        }
                    }
                });
            }
            else if (counter === 2){
                return Bodies.polygon(x, y, 5, 190, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../blender-380-353.png'
                        }
                    }
                })
            }
            else if (counter === 3){
                return Bodies.polygon(x, y, 5, 200, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../c-plus-plus-490.png'
                        }
                    }
                })
            }
            else if (counter === 4) {
                return Bodies.polygon(x, y, 5, 200, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../git-383.png'
                        }
                    }
                })
            }
            else if (counter === 5) {
                return Bodies.polygon(x, y, 5, 250, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../phx-elixir-400.png'
                        }
                    }
                })
            }
            else if (counter === 6) {
                return Bodies.polygon(x, y, 5, 200, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../ubuntu-450.png'
                        }
                    }
                })
            }
            else if (counter === 7) {
                return Bodies.polygon(x, y, 5, 250, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../nodejs-512.png'
                        }
                    }
                })
            }
            else if (counter === 8) {
                return Bodies.polygon(x, y, 5, 75, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../nginx-220-220.png'
                        }
                    }
                })
            }
            else if (counter === 9) {
                return Bodies.polygon(x, y, 5, 128, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../sql-256.png'
                        }
                    }
                })
            }
            else if (counter === 10) {
                return Bodies.polygon(x, y, 5, 180, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../mongodb-400-277.png'
                        }
                    }
                })
            }
            else if (counter === 11) {
                return Bodies.polygon(x, y, 5, 150, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../macos-270.png'
                        }
                    }
                })
            }
            else if (counter === 12) {
                return Bodies.polygon(x, y, 5, 100, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../unity-256.png'
                        }
                    }
                })
            }
            else if (counter === 13) {
                return Bodies.polygon(x, y, 5, 175, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../gimp-304.png'
                        }
                    }
                })
            }
            else if (counter === 13) {
                return Bodies.polygon(x, y, 5, 200, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../java-512.png'
                        }
                    }
                })
            }
            else if (counter === 14) {
                return Bodies.polygon(x, y, 5, 60, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../windows-126.png'
                        }
                    }
                })
            }
            else if (counter === 15) {
                return Bodies.polygon(x, y, 5, 175, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../vagrant-358.png'
                        }
                    }
                })
            }
            else if (counter === 16) {
                return Bodies.polygon(x, y, 5, 175, {
                    render: {
                        strokeStyle: '#ffffff',
                        sprite: {
                            texture: './../c-sharp-264.png'
                        }
                    }
                })
            }

    });

    World.add(world, stack);
    var explosion = function(engine) {
        var bodies = Composite.allBodies(engine.world);

        for (var i = 0; i < bodies.length; i++) {
            var body = bodies[i];

            if (!body.isStatic && body.position.y >= 500) {
                var forceMagnitude = 0.05 * body.mass;

                Body.applyForce(body, body.position, {
                    x: (forceMagnitude + Common.random() * forceMagnitude) * Common.choose([1, -1]),
                    y: -forceMagnitude + Common.random() * -forceMagnitude
                });
            }
        }
    };

    var timeScaleTarget = 1,
        counter = 0;


    Events.on(engine, 'afterUpdate', function(event) {
        // tween the timescale for bullet time slow-mo
        engine.timing.timeScale += (timeScaleTarget - engine.timing.timeScale) * 0.05;

        counter += 1;

        // every 1.5 sec
        if (counter >= 60 * 1.5) {

            // flip the timescale
            if (timeScaleTarget < 1) {
                timeScaleTarget = 1;
            } else {
                timeScaleTarget = 0.05;
            }

            // create some random forces
            explosion(engine);

            // reset counter
            counter = 0;
        }
    });

    var bodyOptions = {
        frictionAir: 0,
        friction: 0.0001,
        restitution: 0.8
    };

    // add mouse control
    var mouse = Mouse.create(render.canvas),
        mouseConstraint = MouseConstraint.create(engine, {
            mouse: mouse,
            constraint: {
                stiffness: 0.2,
                render: {
                    visible: false
                }
            }
        });

    World.add(world, mouseConstraint);

    // keep the mouse in sync with rendering
    render.mouse = mouse;

    // fit the render viewport to the scene
    Render.lookAt(render, {
        min: { x: 0, y: 0 },
        max: { x: 1200, y: 1200 }
    });

    // context for MatterTools.Demo
    return {
        engine: engine,
        runner: runner,
        render: render,
        canvas: render.canvas,
        stop: function() {
            Matter.Render.stop(render);
            Matter.Runner.stop(runner);
        }
    };
};


let instance = Example.timescale();
window.addEventListener('resize', resizeThrottler, false);
var resizeTimeout;
function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
    if ( !resizeTimeout ) {
        resizeTimeout = setTimeout(function() {
            resizeTimeout = null;
            resizeHandler();

            // The actualResizeHandler will execute at a rate of 15fps
        }, 66);
    }
}
function resizeHandler() {
    //instance.stop();
}