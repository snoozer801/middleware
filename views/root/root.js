(function(){

    let navLocation = function(){
        let jQuery = window.$;
        let location = window.location.href.split('/');
        location = location[location.length - 1];
        if (location){
            let navbarButton = jQuery('#' + location + '_nav');
            if (navbarButton){
                navbarButton.css('opacity', '0.5');
            }
        }
    };
    if (window.addEventListener)
    {
        window.addEventListener('load', navLocation, false); // NB **not** 'onload'
    }
    else if (window.attachEvent)
    {
        window.attachEvent('onload', navLocation);
    }
})();