let mongoose = require('mongoose');
const settings = require('./../../settings');

const mongoURL = settings.dev.db.url;
class Database {
    constructor() {
        this._connect()
    }
    _connect() {
        mongoose.connect(mongoURL)
            .then(() => {
                console.log('Database connection successful')
            })
            .catch(err => {
                console.error('Database connection error')
            })
    }
}
module.exports = new Database();