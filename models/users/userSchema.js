let mongoose = require('mongoose');
let validator = require('validator');
let db = require('./../database/db');

let userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: (value) => {
            if (validator.isEmpty(value)){
                return false;
            } else {
                return true;
            }
        }
    },

    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            return validator.isEmail(value)
        }
    },

    lastLogin: {
        type: Date,
    }
},
    {timestamps: true}
    );
module.exports = mongoose.model('User', userSchema);