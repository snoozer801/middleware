let mongoose = require('mongoose');
let validator = require('validator');

let db = require('./../database/db');

let keySchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: (value) => {
            return !validator.isEmpty(value);
        }
    },

    main: {
      type: String,
      required: true,
      validate: (value) => {
          return !validator.isEmpty(value);
      }
    },

    aux: {
        type: String,
        default: "Not Set",
        validate: (value) => {
            return !validator.isEmpty(value);
        }
    },
}, { timestamps: true });
module.exports = mongoose.model('Key', keySchema);