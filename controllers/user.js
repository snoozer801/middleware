const userSchema = require('../models/users/userSchema');

const User = function () {
};

User.findOne = function (username, callback) {
    console.log('finding one', username);
    userSchema.findOne({ username: username }, null, function(err, docs)  {
        if (err) callback(err, null);
        console.log('users found - ', docs);
        if (docs === null){
            return callback(null, {error:'User not found.'});
        }
        callback(null, docs);
    })
};
User.register = function(registration, callback) {
    console.log('registration', registration);
    User.findOne(registration.username, (err, result) => {
        if (err) {
            callback(err);
        }
        if (result.error){
            let user = new userSchema();
            console.log('user saving', user);
            user.username = registration.username;
            user.email = registration.email;
            user.lastLogin = Date.now();
            user.save(function(err, doc){
                console.log('in the saving function');
                console.log(err);
                if (err) return callback(err);
                console.log(doc);
                if (doc){
                  let Key = require('./key');
                  Key.hash( doc._id, registration.password, function (err, res){
                      if (err) return callback(err);
                      console.log('doc after hashing', doc);
                      console.log('hash after', res);
                      callback(null, doc);
                  });
                }
          })
        } else {
            callback(result.error);
        }
    });
};

User.authenticate = function(user, callback){
    const requestedPassword = user.password.toString();
    console.log(user);
    User.findOne(user.username, function(err, discovery) {
        if (err) {
            throw err;
        }
        console.log('type of discovery._id - ', typeof discovery._id);
        console.log('discovery id ', discovery._id);
        if (typeof discovery._id === 'object'){
            let userID = discovery._id.toString();
            let auth = {
                userId: userID,
                password: requestedPassword
            };
            console.log('user id after conv - ', userID);
            console.log('requested password - ', requestedPassword);
            let Key = require('./key');
            Key.compareSync(auth, function(err, result){
                if (err) console.log(err);
                if (result) {
                    discovery.lastLogin = Date.now();
                    discovery.update({ $currentDate: { lastLogin: true }}, function (err) {
                        if (err) console.log('err at 73', err);
                        console.log('new user login successfully saved');
                    });
                    return callback(null, discovery);
                } else {
                    return callback(result);
                }
            });
        } else {
            callback(null, discovery);
        }
    });
};

User.update = function(criteria, callback) {
    let validator = require('validator');
    if (validator.isEmpty(criteria.update)){
        callback({ error: 'Update cannot be empty.' })
    }
    let updateType = criteria.field;
    let query = {}, update = {};
    console.log('is this an emailUpdate?', updateType);
    if (updateType === 'email'){
        query.email = criteria.search;
        if (validator.isEmail(criteria.update)){
            update.email = criteria.update;
        } else {
          return callback({error:'Update must be an email.'})
        }

    } else if (updateType === 'username'){
        query.username = criteria.search;
        update.username = criteria.update;
    }
    console.log('guery', query);
    console.log('update', update);
    userSchema.findOneAndUpdate(query, update, {new:true}, function(err, doc) {
      console.log(err);
      console.log(doc);
        if (err) return callback(err, null);
        if (updateType === 'email' && doc){
            return callback(null, { success: 'Email updated.', update: doc.email, user: doc } );
        } else if (doc) {
            return callback(null, { success: 'Username updated.', update: doc.username, user: doc })
        } else {
            return callback(null, { error: 'You must specify an update value.' })
        }
    })
};

module.exports = User;
