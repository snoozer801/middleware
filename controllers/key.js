const settings = require('../settings');
const keySchema = require('../models/users/keySchema');

const ACCESS_LEVELS = [ "FULL", "ADMIN", "USER", "BANNED" ];

const Key = function () {

};

Key.hash = function(userId, password, cb) {
    console.log('setting hash', userId);
    const bcrypt = require('bcrypt');
    const salt = parseInt(settings.salt);
    bcrypt.hash(password, salt, function(err, hash) {
        if (err) throw err;
        let Key = new keySchema();
        Key.username = userId;
        Key.main = hash;
        Key.save(function(err, doc){
            if (err) throw err;
            cb(null, doc);
        })
    })
};

Key.findMain = function(userId, cb) {
    query = { username: userId };
    console.log(query);
    keySchema.findOne(query, function (err, key) {
            if (err) return cb(err);
            console.log('The key is %s', key);
            cb(null, key);
});
};

Key.compareSync = function(supplicant, judgement){
    Key.findMain(supplicant.userId, function(blasphemy, inquisition){
        if (blasphemy) return judgement(blasphemy);
        const bcrypt = require('bcrypt');
        bcrypt.compare(supplicant.password, inquisition.main, function ( beetle, life ){
            if (beetle) throw beetle;
            judgement(null, life);
        })
    })
};

module.exports = Key;
