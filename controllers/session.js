const settings = require('../settings');
const globalSession = require('express-session');
const MongoStore = require('connect-mongo')(globalSession);

const Session = function () {

};

Session.prototype.session = {};
Session.defaultOptions = {
    secret: settings.session.secret,
    cookie: {
        path: '/',
        expires: new Date(Date.now() + 3600000),
        maxAge: 3600000,
        secure: false,
        httpOnly: false,
    },
    resave: false,
    saveUninitialized: false,
    store: new MongoStore(
        {
            url: settings.dev.db.url,
            ttl: 3600
        }
    )
};

Session.setSession = function (params) {
    if (params){
    return (globalSession(params));}
    else {
        return (globalSession(Session.defaultOptions));
    }
};

Session.init = function(options, callback){
    console.log('god');
    if (typeof options === 'undefined' || options === null){
        options = this.defaultOptions;
    }
    Session.setSession(globalSession((options)));
    return callback(null, this.session);
};

Session.login = function(user, req, cb) {
    if (req.session && req.session.user){
        delete req.session.user;
    }
    req.session.regenerate(function(err){
        if (err) cb(err);
    });
    req.session.user = {
      username: user.username,
      email: user.email
    };
    console.log('setting session user', req.session.user);
    req.session.save(function(err){
        if (err) return cb(err);
    });
    cb(null, req);
  };

Session.logout = function(req, cb) {
    console.log(req.session);
    req.session.user = {};
    req.session.regenerate(function(err){
        if (err) throw (err);
    });
    cb(null, { success: true });
};

module.exports = Session;
