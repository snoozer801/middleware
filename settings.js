require('dotenv').config({silent: true});

module.exports = {
  appPort: process.env.PORT_APP || 3000,
  apiPort: process.env.PORT_API || 5001,
  env: process.env.ENV || 'dev',

  salt: process.env.BCRYPT_SALT || 10,

  // Environment-dependent settings
  dev: {
    db: {
      url: process.env.MONGO_URL_DEV || 'error please set dev db url in env'
    }
  },
  prod: {
    db: {
      url: process.env.MONGO_URL_PROD || 'error please set prod db url in env'
    }
  },

  session: {
    secret: process.env.SESSION_SECRET
  }
};
