const settings = require('./settings');
const Session = require('./controllers/session');
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const os = require('os'); // use this if you need to pull information from the server's machine
const path = require('path');
const fs = require('fs');
const favicon = require('serve-favicon');


app.use(favicon(__dirname + '/resources/public/images/mushroom32.ico'));
app.use(bodyParser.urlencoded({ extended: true })); // extended edition
app.use(express.json()); // enable json format
app.use(express.static(path.join(__dirname, 'resources', 'public', 'images')));
app.use(express.static(path.join(__dirname, 'resources', 'public', 'models')));
app.set('view engine', 'pug'); // set the template engine
app.set('views', path.join(__dirname, 'views/'));
// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.


app.use(Session.setSession(null));
app.use(require('sanitize').middleware);
app.use((req, res, next) => {

    //console.log('matter js loaded', matterjs);
    //console.log(path.join(__dirname, 'resources', 'public', 'images'));
    // log things you want to see with the request here
    //console.log('request session', req.session);
    //console.log('free mem - ' + os.freemem() / 8000000 + ' MB');
    if (req.connection.remoteAddress !== '::ffff:192.168.1.254') {
        console.log('connection', req.connection.remoteAddress + ' ' + Date(Date.now()));
        console.log('connection info', req.originalUrl);
    }
    next();
});
app.get('/', (req, res) => res.status(404).sendFile(__dirname + '/resources/public/html/404.html'));
app.get('/mushroom32.ico', (req, res, next) => {
    console.log('mushroom served');
    res.sendFile(__dirname + '/mushroom32.ico');
    next();
});
const whitelist = ['http://localhost:3000', 'http://127.0.0.1:3000']; // CORS config
const corsOptions = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
};

app.get('/matterjs', (req, res) => {
    res.render('matterjs/matterjs');
});

app.get('/register',(req, res) => {
    if (req.session.user){
        return res.render('register/register', { error: { alreadyRegistered : true, msg: 'You are already registered as ' + req.session.user.username }, user: req.session.user } );
    } else {
      return res.render('register/register', { error: {msg: null }});
    }
});
app.post('/register', (req, res, next) => {
  var registration = req.body;
  if (!registration){
    return res.send({ error: { msg: 'Please provide registration information.' } } );
  }
  if (registration.username.length < 8){
    return res.send({error: 'Username must be at least 8 characters long.'})
  }
  if (registration.password.length < 10){
    return res.send({error: 'Password must be at least 10 characters long.'})
  }
  console.log('hitting line 68')
  let User = require('./controllers/user');
  User.register(registration, function (err, success){
    if (err) {
      console.log(err);
      return res.send({error:err});
    }
    console.log('success is ', success);

    if (success && success.username && success.email){
         Session.login(success, req, function(err, cookies) {
        if (err){
            return res.status(500).send({ error: 'There was an error logging in. Please try again later.'})
        }
        console.log('set cookies - ', cookies);
    });
        req.session.save();
        res.send({success: {authorized: true}});
        next();
    } else {
        return res.send({error: 'Could not register.'})
    }
  });
});
app.get('/logout', (req, res) => {
  Session.logout(req, function(err, res){
    console.log('logout response - ', res);
  });
  req.session.destroy();
  res.redirect('login');
});
app.get('/login', (req, res) => {
  if (req.session && req.session.user){
    res.render('login/login', { user : req.session.user, error: 'Already logged in.' });
  } else {
      res.render('login/login');
  }
});
app.get('/home', (req, res) => {
    res.render('home/home', { user: req.session.user });
});
app.get('/about', (req, res) => {
    res.render('about/about', { user: req.session.user });
})
app.put('/api/users/', (req, res, next) => {
  let updateTerms = req.body;
  console.log('updateTerms', updateTerms);
  let User = require('./controllers/user');
  User.update(updateTerms, function(err, updateStatus){
    if (err) {
      return res.send({error: 'Could not update'})
    }
    else if (updateStatus.errmsg){
      return res.send(updateStatus);
    }
    else if (updateStatus.user && !updateStatus.errmsg) {
      Session.login(updateStatus.user, req, function (err, req){
        if (err) throw err;
        return res.send(updateStatus);
      });
    } else {
      return res.send({error:'Value could not be updated.'})
    }
  })
});

app.post('/api/users/login', (req, res, next) => {
  if (req.body.password && req.body.username){

    let userModel = require('./controllers/user');
    userModel.authenticate(req.body, function(err, response){
      if (response && !response.error){
          Session.login(response, req, function(err, req) {
              if (err){
                  return res.status(500).end({ error: 'There was an error logging in. Please try again later.'})
              }
          });
          if (response.username && response.email){
              res.send({success: {authorized: true}});
              next();
        }} else {
          return res.send({error: 'Invalid Credentials.'});
      }
    });
  } else {
    return res.status(402).send({ error: 'Please provide your credentials.'});
  };
});
app.get('/profile', (req, res, next) => {
  if (req.session.user){
    let User = require('./controllers/user');
    User.findOne(req.session.user.username, function(err, user){
      if(err)throw err;
      res.render('profile/profile', {user: user});
    })
  } else {
    res.redirect('home');
  }
});
//app.options("/products", cors(corsOptions))
app.get('/users/:username', cors(corsOptions), (req, res, next) => { // delet this 🔫
  console.log('serving options')
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  console.log(cors);
  res.json({msg: 'This is CORS-enabled for all origins 🔫'})
})
app.delete('/users/:username', cors(corsOptions), (req, res, next) => {
  console.log('trying to delete')
  res.send('<div style="font-size:300px">👌</div>')
})
app.get('*', function(req, res) {
    res.status(404).sendFile(__dirname + '/resources/public/html/404.html')
});


app.listen(settings.apiPort, () => console.log('Server Ready.'))
